package net.vintagepay.models;

public enum QiwiStatuses {
    DECLINED,
    REPEAT,
    MANUAL,
    OK,
    ACCEPTED
}
