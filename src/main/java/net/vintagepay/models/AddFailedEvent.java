package net.vintagepay.models;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class AddFailedEvent {
    private Boolean isManualException;
}
