package net.vintagepay.models;

public class RequestTypes {
    public static final String EXCHANGE = "exchange";
    public static final String IN = "in";
    public static final String CHECK = "check";
    public static final String WITHDRAW = "withdraw";
    public static final String PING = "ping";
}
