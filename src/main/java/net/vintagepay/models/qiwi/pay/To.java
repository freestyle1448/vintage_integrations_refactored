package net.vintagepay.models.qiwi.pay;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@JsonPropertyOrder({"amount", "ccy", "service-id", "account-number", "extra"})
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class To {
    private String amount;
    private String ccy;
    @JsonProperty("service-id")
    private String service_id;
    @JsonProperty("account-number")
    private String account_number;
    @JsonProperty("extra")
    private Account1 account1;
}
