package net.vintagepay.models.qiwi.pay;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@JsonPropertyOrder({"transaction-number", "from", "to"})
@JsonRootName("payment")
public class Payment {
    @JsonProperty("transaction-number")
    private String transaction_number;
    private To to;
    private From from;
}
