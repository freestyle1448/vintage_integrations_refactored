package net.vintagepay.models.qiwi.pay;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlElement;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class From {
    private String ccy;
    private String amount;

    @XmlElement(name = "ccy")
    public void setCcy(String ccy) {
        this.ccy = ccy;
    }

    @XmlElement(name = "amount")
    public void setAmount(String amount) {
        this.amount = amount;
    }
}
