package net.vintagepay.models.qiwi.pay;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlText;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
class Account1 {
    @JacksonXmlProperty(isAttribute = true, localName = "name")
    private String card;
    @JacksonXmlText
    private String value;


    public void setValue(String value) {
        this.value = value;
    }

    public void setCard(String card) {
        this.card = "account1";
    }
}