package net.vintagepay.models.qiwi.pay;

import com.fasterxml.jackson.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import net.vintagepay.models.qiwi.Password;
import net.vintagepay.models.qiwi.PrivateCredentials;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@JsonRootName("request")
@JsonPropertyOrder({"request-type", "terminal-id", "extra", "auth"})
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@SuppressWarnings("Duplicates")
public class Request {
    @JsonProperty("request-type")
    private String request_type;
    @JsonProperty("terminal-id")
    private String terminal_id;
    @JsonIgnore
    private PrivateCredentials privateCredentials;
    @JsonProperty("extra")
    private Password extra_password;
    private Auth auth;

    @SuppressWarnings("Duplicates")
    public static Request buildRequest(String request_type, String terminal_id,
                                       String passwordStr, String phoneStr, String card, String ccyTo, String ccyFrom,
                                       String amount, String serviceId, String transactionNumber, PrivateCredentials cert) {
        Request request = new Request();
        request.privateCredentials = cert;
        Auth auth = new Auth();
        Payment payment = new Payment();
        To to = new To();
        From from = new From();
        Account1 account1 = new Account1();
        account1.setCard("");
        account1.setValue(card);
        to.setAmount(amount);
        to.setCcy(ccyTo);
        to.setService_id(serviceId);
        to.setAccount_number(phoneStr);
        to.setAccount1(account1);

        from.setCcy(ccyFrom);

        payment.setTransaction_number(transactionNumber);
        payment.setTo(to);
        payment.setFrom(from);

        auth.setPayment(payment);

        request.setRequest_type(request_type);
        request.setTerminal_id(terminal_id);
        Password password = new Password();
        password.setPassword();
        password.setValue(passwordStr);
        request.setExtra_password(password);
        request.setAuth(auth);

        return request;
    }

    public static Request buildRequest(String request_type, String terminal_id, String passwordStr,
                                       String phoneStr, String ccyTo, String ccyFrom, String amount, String serviceId,
                                       String transactionNumber, PrivateCredentials cert) {
        Request request = new Request();
        request.privateCredentials = cert;
        Auth auth = new Auth();
        Payment payment = new Payment();
        To to = new To();
        From from = new From();
        to.setAmount(amount);
        to.setCcy(ccyTo);
        to.setService_id(serviceId);
        to.setAccount_number(phoneStr);

        from.setCcy(ccyFrom);

        payment.setTransaction_number(transactionNumber);
        payment.setTo(to);
        payment.setFrom(from);

        auth.setPayment(payment);

        request.setRequest_type(request_type);
        request.setTerminal_id(terminal_id);
        Password password = new Password();
        password.setPassword();
        password.setValue(passwordStr);
        request.setExtra_password(password);
        request.setAuth(auth);

        return request;
    }

}
