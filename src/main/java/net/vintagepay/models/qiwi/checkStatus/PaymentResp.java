package net.vintagepay.models.qiwi.checkStatus;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@JsonPropertyOrder({"transaction-number", "to"})
public class PaymentResp {
    @JsonProperty("transaction-number")
    private String transaction_number;
    private To to;
}