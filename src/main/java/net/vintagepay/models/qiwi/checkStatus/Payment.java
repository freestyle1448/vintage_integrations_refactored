package net.vintagepay.models.qiwi.checkStatus;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
class Payment {
    private Integer status;
    @JsonProperty("txn-id")
    private Integer txn_id;
    @JsonProperty("transaction-number")
    private String transaction_number;
    @JsonProperty("result-code")
    private Integer result_code;
    @JsonProperty("final-status")
    private Boolean final_status;
    @JsonProperty("fatal-error")
    private Boolean fatal_error;
    @JsonProperty("txn-date")
    private Date txn_date;

}
