package net.vintagepay.models.qiwi.checkStatus;

import com.fasterxml.jackson.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import net.vintagepay.models.qiwi.Password;
import net.vintagepay.models.qiwi.PrivateCredentials;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@JsonPropertyOrder({"request-type", "extra", "terminal-id", "status"})
@JsonRootName("request")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Request {
    @JsonProperty("request-type")
    private String request_type;
    @JsonProperty("extra")
    private Password extra_password;
    @JsonProperty("terminal-id")
    private String terminal_id;
    private Status status;
    @JsonIgnore
    private PrivateCredentials privateCredentials;

    public static Request buildRequest(String account_number, String transaction_number,
                                       String extra_password, String request_type, String terminal_id, PrivateCredentials cert) {
        Request request = new Request();
        request.privateCredentials = cert;
        Status status = new Status();
        PaymentResp payment = new PaymentResp();
        To to = new To();
        to.setAccount_number(account_number);
        payment.setTo(to);
        payment.setTransaction_number(transaction_number);
        status.setPayment(payment);
        request.setStatus(status);
        Password password = new Password();
        password.setPassword();
        password.setValue(extra_password);
        request.setExtra_password(password);
        request.setRequest_type(request_type);
        request.setTerminal_id(terminal_id);

        return request;
    }
}
