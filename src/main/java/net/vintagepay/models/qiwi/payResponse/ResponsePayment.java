package net.vintagepay.models.qiwi.payResponse;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import net.vintagepay.models.qiwi.pay.From;
import net.vintagepay.models.qiwi.pay.To;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ResponsePayment {
    private To to;
    private From from;
    private Integer status;
    @JsonProperty("txn-id")
    private Long txn_id;
    @JsonProperty("transaction-number")
    private String transaction_number;
    @JsonProperty("result-code")
    private Integer result_code;
    @JsonProperty("msg")
    private String msg;
    @JsonProperty("message")
    private String message;
    @JsonProperty("final-status")
    private Boolean final_status;
    @JsonProperty("fatal-error")
    private Boolean fatal_error;
    @JsonProperty("txn-date")
    private String txn_date;
}
