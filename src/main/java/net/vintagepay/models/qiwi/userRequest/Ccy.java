package net.vintagepay.models.qiwi.userRequest;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlText;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
class Ccy {
    private String ccy;
    @JacksonXmlText
    private String value;

    public void setValue(String value) {
        this.value = value;
    }

    public void setCcy() {
        this.ccy = "ccy";
    }
}
