package net.vintagepay.models.qiwi.userRequest;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlText;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlValue;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
class Phone {
    @JacksonXmlProperty(isAttribute = true, localName = "name")
    private String phone;
    @JacksonXmlText
    private String value;

    @XmlValue
    public void setValue(String value) {
        this.value = value;
    }

    @XmlAttribute(name = "name")
    public void setPhone(String phone) {
        this.phone = "receiver";
    }
}
