package net.vintagepay.models.qiwi.userRequest;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import net.vintagepay.models.qiwi.Password;


@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@JsonPropertyOrder({"request-type", "terminal-id", "extra-password", "extra-phone", "extra-ccy"})
@JsonRootName("request")
public class QiwiUserCheckRequest {
    @JsonProperty("request-type")
    private String request_type;
    @JsonProperty("terminal-id")
    private String terminal_id;
    private Password extra_password;
    private Phone extra_phone;
    private Ccy extra_ccy;

    public static QiwiUserCheckRequest buildRequest(String request_type, String terminal_id, String password, String phone, String ccy) {
        QiwiUserCheckRequest qiwiUserCheckRequest = new QiwiUserCheckRequest();
        qiwiUserCheckRequest.setTerminal_id(terminal_id);
        qiwiUserCheckRequest.setRequest_type(request_type);
        Password passwordP = new Password();
        passwordP.setPassword();
        passwordP.setValue(password);
        qiwiUserCheckRequest.setExtra_password(passwordP);
        Ccy ccyC = new Ccy();
        ccyC.setCcy();
        ccyC.setValue(ccy);
        qiwiUserCheckRequest.setExtra_ccy(ccyC);
        Phone phoneP = new Phone();
        phoneP.setPhone("");
        phoneP.setValue(phone);
        qiwiUserCheckRequest.setExtra_phone(phoneP);


        return qiwiUserCheckRequest;
    }
}
