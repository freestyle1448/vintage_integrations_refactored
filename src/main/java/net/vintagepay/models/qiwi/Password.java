package net.vintagepay.models.qiwi;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlText;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
public class Password {
    @JacksonXmlProperty(isAttribute = true, localName = "name")
    private String password;
    @JacksonXmlText
    private String value;

    public void setValue(String value) {
        this.value = value;
    }

    public void setPassword() {
        this.password = "password";
    }
}
