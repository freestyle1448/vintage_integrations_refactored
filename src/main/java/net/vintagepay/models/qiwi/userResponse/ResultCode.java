package net.vintagepay.models.qiwi.userResponse;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlText;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class ResultCode {
    private Boolean fatal;
    @JacksonXmlText
    private String value;
    private String message;
    private String msg;
}
