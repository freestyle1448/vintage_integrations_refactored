package net.vintagepay.models.qiwi.userResponse;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
class Response {
    private String exist;
    @JsonProperty("result-code")
    private ResultCode resultCode;
}
