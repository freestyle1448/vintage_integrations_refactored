package net.vintagepay.models.qiwi.ping;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;
import lombok.ToString;
import net.vintagepay.models.qiwi.Password;
import net.vintagepay.models.qiwi.PrivateCredentials;
import org.bson.types.ObjectId;


@JsonPropertyOrder({"request-type", "terminal-id", "extra-password"})
@JsonRootName("request")
@ToString
@Data
public class Request {
    @JsonProperty("request-type")
    private String request_type;
    @JsonProperty("extra")
    private Password extra_password;
    @JsonProperty("terminal-id")
    private String terminal_id;

    @JsonIgnore
    private ObjectId objectId;
    @JsonIgnore
    private String currency;
    @JsonIgnore
    private PrivateCredentials privateCredentials;

    public Request() {
    }

    public static Request buildRequest(String extra_password, String request_type, String terminal_id) {
        Request request = new Request();
        Password password = new Password();
        password.setPassword();
        password.setValue(extra_password);
        request.setExtra_password(password);
        request.setRequest_type(request_type);
        request.setTerminal_id(terminal_id);

        return request;
    }
}
