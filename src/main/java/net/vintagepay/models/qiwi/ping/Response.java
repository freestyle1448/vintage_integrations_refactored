package net.vintagepay.models.qiwi.ping;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import net.vintagepay.models.qiwi.userResponse.ResultCode;
import net.vintagepay.models.qiwi.payResponse.Balance;
import org.bson.types.ObjectId;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@JsonRootName("response")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Response {
    @JsonProperty("result-code")
    private ResultCode result_code;
    @JsonIgnore
    private String currency;
    @JsonIgnore
    private ObjectId objectId;
    @JacksonXmlElementWrapper(localName = "balances")
    private List<Balance> balances;
    @JacksonXmlElementWrapper(localName = "balances-ovd")
    private List<BalanceOvd> balancesOvd;
}
