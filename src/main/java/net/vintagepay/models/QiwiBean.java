package net.vintagepay.models;

import net.vintagepay.models.qiwi.payResponse.QiwiResponse;
import net.vintagepay.models.qiwi.ping.Response;
import net.vintagepay.models.transaction.Balance;
import net.vintagepay.models.transaction.Status;
import net.vintagepay.models.transaction.Transaction;
import net.vintagepay.repositories.ManualRepository;
import net.vintagepay.services.SystemService;
import net.vintagepay.tasks.QiwiTasks;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.concurrent.CompletableFuture;

import static net.vintagepay.services.QiwiServiceImpl.getCurrency;

@Component
public class QiwiBean {
    private static final String BAD_MES = "Списание средств неуспешно для транзакции - {}, статус - {}, сообщение - {}";
    private static final String BANK_DECLINED = "Платеж отклонён банком";

    private static final Logger logger = LoggerFactory.getLogger(QiwiTasks.class);

    private final SystemService systemService;
    private final ManualRepository manualRepository;

    public QiwiBean(SystemService systemService, ManualRepository manualRepository) {
        this.systemService = systemService;
        this.manualRepository = manualRepository;
    }

    public void handlePing(Response ping) {
        ping.getBalances().forEach(balance -> {
            if (balance.getCode().equals(ping.getCurrency())) {
                manualRepository.findAndModifyGateSet(ping.getObjectId(), Balance
                        .builder()
                        .amount((long) (Double.parseDouble(balance.getValue()) * 100))
                        .build());
            }
        });
    }

    public CompletableFuture<Transaction> handleWithdraw(QiwiResponse response1, Transaction transaction) {

        if (response1.getPayment() != null) {
            final var isFatal = response1.getPayment().getFatal_error();
            final var isFinal = response1.getPayment().getFinal_status();
            final var status = response1.getPayment().getStatus();
            final var message = response1.getPayment().getMessage();

            switch (checkQiwiStatusOnWithdraw(isFatal, isFinal, status)) {
                case DECLINED:
                    logger.info(BAD_MES
                            , transaction, status, message);
                    systemService.declineTransaction(transaction, BANK_DECLINED, message);
                    break;
                case REPEAT:
                    systemService.saveTransaction(transaction, 3, Status.IN_PROCESS);
                    break;
                case MANUAL:
                    logger.info("Списание требует ручной обработки - {}, статус - {}"
                            , transaction, status);
                    systemService.saveTransaction(transaction, 0, Status.MANUAL, "Ожидает подтверждения Банка", message);
                    break;
                case OK:
                    logger.info("Заявка на списание средств успешно для транзакции - {}, статус - {}"
                            , transaction, status);

                    systemService.saveTransaction(transaction, 1, Status.IN_PROCESS);
                    updateQiwiBalance(response1, transaction);
                    break;
                case ACCEPTED:
                    handleSuccessOperation(response1, transaction);
                    break;
            }

            return CompletableFuture.completedFuture(transaction);
        } else {
            if (response1.getResult_code() != null) {
                if (!response1.getResult_code().getValue().equals("0")) {
                    logger.info("Списание средств неуспешно для транзакции - {}, статус - {}"
                            , transaction, response1.getResult_code().getValue());

                    systemService.declineTransaction(transaction, BANK_DECLINED, response1.getResult_code().getValue());
                } else {
                    systemService.saveTransaction(transaction, 3, Status.IN_PROCESS);
                }

                return CompletableFuture.completedFuture(transaction);
            }
        }

        return CompletableFuture.completedFuture(transaction);
    }

    public CompletableFuture<Transaction> handleCheckTransaction(QiwiResponse response1, Transaction transaction) {
        if (response1.getPayment() != null) {
            final var isFatal = response1.getPayment().getFatal_error();
            final var isFinal = response1.getPayment().getFinal_status();
            final var status = response1.getPayment().getStatus();
            final var message = response1.getPayment().getMessage();

            if (response1.getResult_code().getValue().equals("0")) {
                switch (checkQiwiStatusOnCheck(isFatal, isFinal, status)) {
                    case DECLINED:
                        logger.info(BAD_MES
                                , transaction, status, message);

                        systemService.declineTransaction(transaction, BANK_DECLINED, message);
                        break;
                    case MANUAL:
                        logger.info("Списание требует ручной обработки - {}, статус - {}"
                                , transaction, status);

                        systemService.saveTransaction(transaction, 0, Status.MANUAL, "Ожидает подтверждения Банка", message);
                        break;
                    case OK:
                        logger.info("Заявка на списание средств ждёт выполнения для транзакции - {}, статус - {}"
                                , transaction, response1.getPayment().getStatus());
                        break;
                    case ACCEPTED:
                        handleSuccessOperation(response1, transaction);
                        break;
                }
            } else {
                logger.info(BAD_MES
                        , transaction, status, message);

                systemService.declineTransaction(transaction, BANK_DECLINED, message);
            }
        }

        return CompletableFuture.completedFuture(transaction);
    }

    private void handleSuccessOperation(QiwiResponse response1, Transaction transaction) {
        updateQiwiBalance(response1, transaction);

        systemService.confirmTransaction(transaction);

        logger.info("Списание средств успешно для транзакции - {}, статус - {}"
                , transaction, response1.getPayment().getStatus());
    }

    public void updateQiwiBalance(QiwiResponse response1, Transaction transaction) {
        final var transactionCurrency = String.valueOf(getCurrency(transaction.getAmount().getCurrency()));
        response1.getBalances().forEach(balance -> {
            if (balance.getCode().equals(transactionCurrency)) {
                manualRepository.findAndModifyGateSet(transaction.getGateId(), Balance.builder()
                        .currency(transaction.getAmount().getCurrency())
                        .amount((long) (Double.parseDouble(balance.getValue()) * 100))
                        .build());
            }
        });
    }

    private QiwiStatuses checkQiwiStatusOnCheck(boolean isFatal, boolean isFinal, int status) {
        if (isFatal) {
            return QiwiStatuses.DECLINED;
        }

        if (isFinal) {
            if (status == 60) {
                return QiwiStatuses.ACCEPTED;
            }
            if (status > 99) {
                return QiwiStatuses.DECLINED;
            }
        } else {
            if (status == -1) {
                return QiwiStatuses.OK;
            }
            if (status > -1 && status < 50) {
                return QiwiStatuses.MANUAL;
            }
            if (status > 49 && status < 60) {
                return QiwiStatuses.OK;
            }
        }

        return QiwiStatuses.DECLINED;
    }

    private QiwiStatuses checkQiwiStatusOnWithdraw(boolean isFatal, boolean isFinal, int status) {
        if (isFatal) {
            return QiwiStatuses.DECLINED;
        }

        if (isFinal) {
            if (status == 60) {
                return QiwiStatuses.ACCEPTED;
            }
            if (status > 99) {
                return QiwiStatuses.DECLINED;
            }
        } else {
            if (status == -1) {
                return QiwiStatuses.DECLINED;
            }
            if (status > -1 && status < 50) {
                return QiwiStatuses.MANUAL;
            }
            if (status > 49 && status < 60) {
                return QiwiStatuses.OK;
            }
        }

        return QiwiStatuses.DECLINED;
    }
}
