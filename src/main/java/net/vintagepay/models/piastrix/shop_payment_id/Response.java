package net.vintagepay.models.piastrix.shop_payment_id;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
class Response {
    private Integer id;
    private Integer payee_receive;
    private Integer ps_currency;
    private Integer shop_currency;
    private String shop_payment_id;
    private Double shop_write_off;
    private Integer status;
}
