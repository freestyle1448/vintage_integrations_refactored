package net.vintagepay.models.piastrix.shop_payment_id;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
class Request {
    private Integer shop_id;
    private String shop_payment_id;
    private String now;
    private String sign;
}
