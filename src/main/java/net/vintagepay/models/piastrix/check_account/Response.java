package net.vintagepay.models.piastrix.check_account;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Response {
    private DataCheck data;
    private Integer error_code;
    private String message;
    private Boolean result;
}
