package net.vintagepay.models.piastrix.withdraw_id;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import net.vintagepay.app.PiastrixProps;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Request {
    private String now;
    private Integer shop_id;
    private Integer withdraw_id;
    private String sign;

    public void genSign() {
        final String originalString = now + ":" + shop_id + ":" + withdraw_id + PiastrixProps.SECRET_KEY;

    }
}
