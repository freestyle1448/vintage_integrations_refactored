package net.vintagepay.models.piastrix.withdraw_id;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class DataWithDrawId {
    private Integer id;
    private Double payeeReceive;
    private Integer psCurrency;
    private Integer shopCurrency;
    private String shopPaymentId;
    private Integer status;
}
