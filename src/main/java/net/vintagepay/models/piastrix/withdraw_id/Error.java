package net.vintagepay.models.piastrix.withdraw_id;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
class Error {
    private DataId data;
    private Integer error_code;
    private String message;
    private Boolean result;
}
