package net.vintagepay.models.piastrix.shop_balance;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

import java.util.List;

@lombok.Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Data {
    private Integer shop_id;
    private List<Balance> balances;
}
