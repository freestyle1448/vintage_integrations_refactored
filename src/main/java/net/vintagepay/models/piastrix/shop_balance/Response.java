package net.vintagepay.models.piastrix.shop_balance;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Response {
    private net.vintagepay.models.piastrix.shop_balance.Data data;
    private Boolean result;
    private Integer error_code;
    private String message;
}
