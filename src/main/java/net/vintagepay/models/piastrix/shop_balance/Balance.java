package net.vintagepay.models.piastrix.shop_balance;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Balance {
    private Number available;
    private Number hold;
    private Number frozen;
    private Integer currency;
}
