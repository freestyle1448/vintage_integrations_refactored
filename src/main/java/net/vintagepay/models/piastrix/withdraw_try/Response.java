package net.vintagepay.models.piastrix.withdraw_try;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Response {
    private DataTry data;
    private Integer error_code;
    private String message;
    private Boolean result;
}
