package net.vintagepay.models.piastrix.withdraw_try;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
class DataTry {
    private AccountInfoConfig account_info_config;
    private Double payee_receive;
    private Integer ps_currency;
    private Integer shop_currency;
    private Double shop_write_off;
}
