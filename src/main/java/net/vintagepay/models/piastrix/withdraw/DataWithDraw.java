package net.vintagepay.models.piastrix.withdraw;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class DataWithDraw {
    private Double balance;
    private Integer id;
    private Double payee_receive;
    private Integer ps_currency;
    private Integer shop_currency;
    private String shop_payment_id;
    private Double shop_write_off;
    private Integer status;
}
