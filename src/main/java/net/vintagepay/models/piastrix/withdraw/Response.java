package net.vintagepay.models.piastrix.withdraw;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Response {
    private DataWithDraw data;
    private Integer error_code;
    private String message;
    private Boolean result;
}
