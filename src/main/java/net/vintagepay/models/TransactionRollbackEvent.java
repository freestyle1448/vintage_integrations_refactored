package net.vintagepay.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Value;
import net.vintagepay.models.transaction.Transaction;

@Data
@AllArgsConstructor
public class TransactionRollbackEvent {
    private Transaction transaction;
    private Boolean isManualException;
}
