package net.vintagepay.models;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.Getter;
import lombok.Setter;
import net.vintagepay.models.qiwi.PrivateCredentials;
import net.vintagepay.models.qiwi.pay.Request;
import net.vintagepay.models.qiwi.payResponse.QiwiResponse;
import net.vintagepay.models.qiwi.ping.Response;
import net.vintagepay.repositories.QiwiErrorsRepository;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.Date;
import java.util.concurrent.CompletableFuture;

@Component
@ConfigurationProperties("qiwi")
@Getter
@Setter
public class QiwiHTTPRequest {
    private static final Logger logger = LoggerFactory.getLogger(QiwiHTTPRequest.class);
    private final QiwiErrorsRepository qiwiErrorsRepository;
    private final ObjectMapper xmlMapper = new XmlMapper().enable(SerializationFeature.INDENT_OUTPUT);

    private final RestTemplate restTemplate;
    private String createUrl;
    private String checkUrl;
    private String pingUrl;

    public QiwiHTTPRequest(QiwiErrorsRepository qiwiErrorsRepository, RestTemplate restTemplate) {
        this.qiwiErrorsRepository = qiwiErrorsRepository;
        this.restTemplate = restTemplate;
    }

    public CompletableFuture<QiwiResponse> withdrawQiwi(Request request) {
        String stringRequest = null;

        try {
            stringRequest = xmlMapper.writeValueAsString(request);
        } catch (JsonProcessingException e) {
            logger.error("Ошибка при парсинге запроса к qiwi", e);

            return CompletableFuture.completedFuture(null);
        }

        final var qiwiRequest = createQiwiRequest(stringRequest, request.getPrivateCredentials());
        String stringResponse;
        try {
            stringResponse = restTemplate.postForObject(createUrl, qiwiRequest, String.class);
        } catch (Exception ex) {
            logger.error("При отправке запроса к QIWI", ex);
            qiwiErrorsRepository.save(QiwiRunnerError.builder()
                    .transactionNumber(request.getAuth().getPayment().getTransaction_number())
                    .date(new Date())
                    .requestType(RequestTypes.WITHDRAW)
                    .request(qiwiRequest.toString())
                    .fullException(ex.fillInStackTrace().toString())
                    .build());

            return CompletableFuture.completedFuture(null);
        }

        logger.info("Запрос к qiwi на вывод - {}", qiwiRequest);
        logger.info("Ответ от qiwi на вывод - {}", stringResponse);
        qiwiErrorsRepository.save(QiwiRunnerError.builder()
                .transactionNumber(request.getAuth().getPayment().getTransaction_number())
                .date(new Date())
                .requestType(RequestTypes.WITHDRAW)
                .request(qiwiRequest.toString())
                .response(stringResponse)
                .build());

        return CompletableFuture.completedFuture(createQiwiResponse(stringResponse));
    }

    public CompletableFuture<QiwiResponse> checkTransactionQiwi(net.vintagepay.models.qiwi.checkStatus.Request check, Request request, Long transactionNumber) {
        String stringRequest = null;

        try {
            if (request != null) {
                stringRequest = xmlMapper.writeValueAsString(request);
            } else {
                stringRequest = xmlMapper.writeValueAsString(check);
            }
        } catch (JsonProcessingException e) {
            logger.error("Ошибка при парсинге запроса к qiwi", e);
        }

        HttpEntity<MultiValueMap<String, String>> qiwiRequest;
        if (check != null) {
            qiwiRequest = createQiwiRequest(stringRequest, check.getPrivateCredentials());
        } else {
            qiwiRequest = createQiwiRequest(stringRequest, request.getPrivateCredentials());
        }
        String stringResponse = restTemplate.postForObject(checkUrl, qiwiRequest, String.class);

        logger.info("Запрос к qiwi на проверку статуса - {}", qiwiRequest);
        logger.info("Ответ от qiwi на проверку статуса - {}", stringResponse);
        qiwiErrorsRepository.save(QiwiRunnerError.builder()
                .date(new Date())
                .transactionNumber(transactionNumber.toString())
                .requestType(RequestTypes.CHECK)
                .request(qiwiRequest.toString())
                .response(stringResponse)
                .build());

        return CompletableFuture.completedFuture(createQiwiResponse(stringResponse));
    }

    public CompletableFuture<Response> ping(net.vintagepay.models.qiwi.ping.Request ping) {
        String stringRequest = null;

        try {
            stringRequest = xmlMapper.writeValueAsString(ping);
        } catch (JsonProcessingException e) {
            logger.error("Ошибка при парсинге запроса к qiwi", e);
        }

        final var pingRequest = createQiwiRequest(stringRequest, ping.getPrivateCredentials());

        String stringResponse;
        try {
            stringResponse = restTemplate.postForObject(pingUrl, pingRequest, String.class);

        } catch (Exception ex) {
            logger.error("При отправке запроса к QIWI", ex);
            qiwiErrorsRepository.save(QiwiRunnerError.builder()
                    .date(new Date())
                    .requestType(RequestTypes.PING)
                    .fullException(ex.fillInStackTrace().toString())
                    .build());

            return CompletableFuture.completedFuture(null);
        }

        logger.info("Запрос к qiwi на проверку баланса - {}", pingRequest);
        logger.info("Ответ от qiwi на проверку баланса - {}", stringResponse);
        qiwiErrorsRepository.save(QiwiRunnerError.builder()
                .date(new Date())
                .requestType(RequestTypes.PING)
                .request(pingRequest.toString())
                .response(stringResponse)
                .build());

        return CompletableFuture.completedFuture(createQiwiResponsePing(stringResponse, ping.getObjectId(), ping.getCurrency()));
    }

    private HttpEntity<MultiValueMap<String, String>> createQiwiRequest(String result, PrivateCredentials privateCred) {
        //----Получение ответа
        result = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n".concat(result);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        map.add("payload", result);
        map.add("certificate", privateCred.getCertificate());
        map.add("privateKey", privateCred.getPrivateKey());

        return new HttpEntity<>(map, headers);
    }

    private QiwiResponse createQiwiResponse(String response) {
        QiwiResponse qiwiResponse;
        if (response == null)
            return null;
        try {
            qiwiResponse = xmlMapper.readValue(response, QiwiResponse.class);
        } catch (JsonProcessingException e) {
            logger.error("При парсинге ответа от qiwi, ", e);

            return null;
        }

        return qiwiResponse;
    }

    private Response createQiwiResponsePing(String response, ObjectId objectId, String currency) {
        Response ping;

        if (response == null)
            return null;

        try {
            ping = xmlMapper.readValue(response, Response.class);
        } catch (JsonProcessingException e) {
            logger.error("При парсинге ответа от qiwi на запрос баланса, ", e);

            return null;
        }

        ping.setObjectId(objectId);
        ping.setCurrency(currency);

        return ping;
    }
}
