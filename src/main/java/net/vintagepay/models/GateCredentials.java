package net.vintagepay.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class GateCredentials {
    private String terminal_id;
    private String password;
    private String certificate;
    private String privateKey;
}
