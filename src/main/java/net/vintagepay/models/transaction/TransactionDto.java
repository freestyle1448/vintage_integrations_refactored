package net.vintagepay.models.transaction;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.*;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString

public class TransactionDto {
    private static final Logger logger = LoggerFactory.getLogger(TransactionDto.class);

    private String salt;
    private String sign;
    private String gateId;
    private String amount;
    private String purpose;
    private String senderCredentials;
    private String receiverCredentials;

    public Transaction createTransaction(String type) {
        try {
            return Transaction.builder()
                    .sign(sign)
                    .salt(salt)
                    .gateId(gateId != null ? new ObjectId(gateId) : null)
                    .startDate(new Date())
                    .transactionNumber(System.currentTimeMillis())
                    .stage(0)
                    .type(type)
                    .amount(amount != null ? Balance.builder()
                            .amount(Long.valueOf(amount))
                            .build() : null)
                    .finalAmount(Balance.builder().build())
                    .commission(Balance.builder().build())
                    .purpose(purpose)
                    .receiverCredentials(receiverCredentials != null ? new ObjectMapper().readValue(receiverCredentials, UserCredentials.class) : null)
                    .senderCredentials(senderCredentials != null ? new ObjectMapper().readValue(senderCredentials, UserCredentials.class) : null)
                    .build();
        } catch (Exception e) {

            logger.error("Ошибка при разборе полученных данных!", e);
            return null;
        }
    }
}
