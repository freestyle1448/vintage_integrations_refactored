package net.vintagepay.models.transaction;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import net.vintagepay.exception.IllegalValueException;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Balance {
    private Long amount;
    private String currency;

    public Balance add(Balance add2) {
        if (!this.getCurrency().equals(add2.getCurrency()))
            throw new IllegalValueException("Валюты должны быть равны");

        return Balance.builder()
                .currency(this.getCurrency())
                .amount(this.getAmount() + add2.getAmount())
                .build();
    }
}
