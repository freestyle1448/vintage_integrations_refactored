package net.vintagepay.controller;

import lombok.RequiredArgsConstructor;
import net.vintagepay.models.Answer;
import net.vintagepay.models.transaction.Balance;
import net.vintagepay.services.SystemService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.Callable;

@RestController
@RequiredArgsConstructor
public class SystemController {
    private final SystemService systemService;

    @PostMapping("/api/account/add")
    public Callable<Answer> accountAdd(Long amount, String currency, String account) {
        return () -> systemService.accountAdd(Balance.builder()
                .amount(amount)
                .currency(currency)
                .build(), account).get();
    }
}
