package net.vintagepay.exception;

public class VintageException extends RuntimeException {
    public VintageException(String message) {
        super(message);
    }
}