package net.vintagepay.exception;

import lombok.Data;
import lombok.EqualsAndHashCode;


public class NotEnoughMoneyException extends RuntimeException {
    public NotEnoughMoneyException(String message) {
        super(message);
    }
}
