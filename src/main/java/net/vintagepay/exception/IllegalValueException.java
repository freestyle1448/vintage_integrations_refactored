package net.vintagepay.exception;

public class IllegalValueException extends VintageException {
    public IllegalValueException(String message) {
        super(message);
    }
}
