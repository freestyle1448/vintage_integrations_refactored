package net.vintagepay.aspect;

import com.mongodb.MongoException;
import lombok.RequiredArgsConstructor;
import net.vintagepay.exception.NotEnoughMoneyException;
import net.vintagepay.exception.NotFoundException;
import net.vintagepay.models.TransactionRollbackEvent;
import net.vintagepay.models.transaction.Transaction;
import net.vintagepay.tasks.QiwiTasks;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Component;

@Aspect
@Component
@RequiredArgsConstructor
public class UserServiceAspect {
    private static final Logger logger = LoggerFactory.getLogger(QiwiTasks.class);
    private final ApplicationEventPublisher applicationEventPublisher;

    @Around("execution (* net.vintagepay.services.UserServiceImpl.createExchangeTransaction(..)) && args(transaction,..)")
    public Object loggingExchangeTransaction(ProceedingJoinPoint joinPoint, Transaction transaction) throws Throwable {
        logger.info("Начало перевода");

        Object proceed;
        final var event = new TransactionRollbackEvent(transaction, false);
        try {
            applicationEventPublisher.publishEvent(event);
            proceed = joinPoint.proceed();
        } catch (NotEnoughMoneyException ex) {
            logger.error("Недостаточно средств на аккаунте отправителя! Для транзакции {}", transaction);

            event.setIsManualException(true);
            throw ex;
        } catch (NotFoundException ex) {
            logger.error("{} Для транзакции {}", ex.getLocalizedMessage(), transaction);

            event.setIsManualException(true);
            throw ex;
        } catch (MongoException | DataAccessException ex) {
            throw ex;
        } catch (Exception ex) {
            logger.error("Ошибка {}! Для транзакции {}", ex, transaction);

            event.setIsManualException(true);
            throw ex;
        }

        logger.info("Закончили перевод для - {}", transaction);
        return proceed;
    }
}
