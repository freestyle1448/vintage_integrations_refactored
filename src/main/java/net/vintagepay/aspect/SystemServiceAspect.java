package net.vintagepay.aspect;

import com.mongodb.MongoException;
import lombok.RequiredArgsConstructor;
import net.vintagepay.exception.NotEnoughMoneyException;
import net.vintagepay.exception.NotFoundException;
import net.vintagepay.models.Answer;
import net.vintagepay.models.TransactionRollbackEvent;
import net.vintagepay.models.transaction.Transaction;
import net.vintagepay.tasks.QiwiTasks;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Component;

import java.util.concurrent.CompletableFuture;

@Aspect
@Component
@RequiredArgsConstructor
public class SystemServiceAspect {
    private static final Logger logger = LoggerFactory.getLogger(QiwiTasks.class);
    private final ApplicationEventPublisher applicationEventPublisher;

    @Around("execution (* net.vintagepay.services.SystemServiceImpl.accountSub(..)) && args(transaction,..)")
    public Object loggingAccountSub(ProceedingJoinPoint joinPoint, Transaction transaction) throws Throwable {
        logger.info("Начало списания средств с аккаунта");

        Object proceed;
        final var event = new TransactionRollbackEvent(transaction, false);
        try {
            applicationEventPublisher.publishEvent(event);
            proceed = joinPoint.proceed();
        } catch (NotEnoughMoneyException ex) {
            logger.error("Недостаточно средств на аккаунте отправителя! Для транзакции {}", transaction);

            event.setIsManualException(true);
            throw ex;
        } catch (NotFoundException ex) {
            logger.error("Аккаунт с указанным ID не найден! Для транзакции {}", transaction);

            event.setIsManualException(true);
            throw ex;
        } catch (MongoException | DataAccessException ex) {
            throw ex;
        } catch (Exception ex) {
            logger.error("Ошибка {}! Для транзакции {}", ex, transaction);

            event.setIsManualException(true);
            throw ex;
        }

        logger.info("Закончили списание для - {}", transaction);
        return proceed;
    }

    @Around("execution(* net.vintagepay.services.SystemServiceImpl.declineTransaction(..)) && args(transaction,..)")
    public Object loggingDecline(ProceedingJoinPoint joinPoint, Transaction transaction) throws Throwable {
        logger.info("Начало отмены транзакции");

        Object proceed;
        final var event = new TransactionRollbackEvent(transaction, false);
        try {
            applicationEventPublisher.publishEvent(event);
            proceed = joinPoint.proceed();
        } catch (NotFoundException ex) {
            logger.error("Во время отмены транзакции аккаунт не был найден! - {}", transaction);

            event.setIsManualException(true);
            throw ex;
        } catch (MongoException | DataAccessException ex) {
            throw ex;
        } catch (Exception ex) {
            logger.error("Ошибка {}! Для транзакции {}", ex, transaction);

            event.setIsManualException(true);
            throw ex;
        }

        logger.error("Успешный возврат! - id- {}", transaction);
        return proceed;
    }

    @Around("execution(* net.vintagepay.services.SystemServiceImpl.accountAdd(..))")
    public Object loggingAccountAdd(ProceedingJoinPoint joinPoint) throws Throwable {
        logger.info("Обработка операции на ввод");

        Object proceed;
        try {
            proceed = joinPoint.proceed();
        } catch (NotFoundException ex) {
            logger.error("{}\n", ex.getLocalizedMessage());

            throw ex;
        } catch (MongoException | DataAccessException ex) {
            throw ex;
        } catch (Exception ex) {
            logger.error("Пополнение не удалось!", ex);

            throw ex;
        }

        logger.info("Пополнение аккаунта успешно");

        return proceed;
    }
}
