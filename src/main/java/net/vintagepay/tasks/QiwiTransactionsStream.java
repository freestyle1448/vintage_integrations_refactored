package net.vintagepay.tasks;

import com.mongodb.client.model.changestream.ChangeStreamDocument;
import com.mongodb.client.model.changestream.FullDocument;
import net.vintagepay.models.QiwiRunnerError;
import net.vintagepay.models.RequestTypes;
import net.vintagepay.models.transaction.Transaction;
import net.vintagepay.repositories.GatesRepository;
import net.vintagepay.repositories.ManualRepository;
import net.vintagepay.repositories.QiwiErrorsRepository;
import net.vintagepay.services.RunnerService;
import net.vintagepay.services.UserService;
import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.messaging.ChangeStreamRequest;
import org.springframework.data.mongodb.core.messaging.MessageListener;
import org.springframework.data.mongodb.core.messaging.MessageListenerContainer;
import org.springframework.data.mongodb.core.messaging.Subscription;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

import static net.vintagepay.models.transaction.Status.WAITING;
import static net.vintagepay.models.transaction.Type.*;

@Component
public class QiwiTransactionsStream {
    public static final String STATUS = "status";
    public static final String STAGE = "stage";
    public static final String TRANSACTIONS = "transactions";

    private static final Logger logger = LoggerFactory.getLogger(QiwiTasks.class);
    private final QiwiErrorsRepository qiwiErrorsRepository;

    private final GatesRepository gatesRepository;
    private final ManualRepository manualRepository;
    private final MessageListenerContainer messageListenerContainer;
    private final RunnerService qiwiService;
    private final UserService userService;
    private final QiwiTasks qiwiTasks;

    public QiwiTransactionsStream(GatesRepository gatesRepository, ManualRepository manualRepository, MessageListenerContainer messageListenerContainer,
                                  @Qualifier("qiwiServiceImpl") RunnerService qiwiService, UserService userService, QiwiTasks qiwiTasks, QiwiErrorsRepository qiwiErrorsRepository) {
        this.gatesRepository = gatesRepository;
        this.manualRepository = manualRepository;
        this.messageListenerContainer = messageListenerContainer;
        this.qiwiService = qiwiService;
        this.userService = userService;
        this.qiwiTasks = qiwiTasks;
        this.qiwiErrorsRepository = qiwiErrorsRepository;
    }


    @PostConstruct
    public void postConstruct() {
        try {
            qiwiTasks.pay();
            qiwiTasks.exchange();
        } catch (Exception e) {
            logger.error("Ошибка при вызове метода восстановления после сбоя!", e);
        }

        System.out.println(subscribeToQiwiOut().isActive());
        System.out.println(subscribeToExchange().isActive());
    }

    public Subscription subscribeToQiwiOut() {
        final var qiwiOutTransaction = Aggregation.newAggregation(Aggregation.match(Criteria
                .where(STATUS).is(WAITING)
                .and(STAGE).is(0)
                .and("type").is(USER_OUT)));

        MessageListener<ChangeStreamDocument<Document>, Transaction> messageListener = message -> {
            try {
                Transaction newTransaction = message.getBody();
                if (newTransaction != null) {
                    final var gateOptional = gatesRepository.findById(newTransaction.getGateId());

                    if (gateOptional.isPresent()) {
                        final var gate = gateOptional.get();

                        if (gate.getGroup().equals("qiwi")) {
                            final var qiwiTransactionUpdated = manualRepository.findAndModifyQiwiTransaction(newTransaction.getId());
                            logger.info("Вызов метода out для транзакции - {}", newTransaction);

                            qiwiService.withdraw(qiwiTransactionUpdated, gate);
                        }
                    }
                }
            } catch (Exception ex) {
                qiwiErrorsRepository.save(QiwiRunnerError.builder()
                        .error(ex.toString())
                        .fullException(ex.fillInStackTrace().toString())
                        .requestType(RequestTypes.WITHDRAW)
                        .build());
            }
        };

        ChangeStreamRequest<Transaction> request = ChangeStreamRequest.builder(messageListener)
                .collection(TRANSACTIONS)
                .filter(qiwiOutTransaction)
                .build();

        messageListenerContainer.start();
        return messageListenerContainer.register(request, Transaction.class);
    }

    public Subscription subscribeToExchange() {
        final var excTransaction = Aggregation.newAggregation(Aggregation.match(Criteria
                .where(STATUS).is(WAITING)
                .and(STAGE).is(0)
                .and("type").is(USER_EXCHANGE)));

        MessageListener<ChangeStreamDocument<Document>, Transaction> messageListener = message -> {
            try {
                Transaction newTransaction = message.getBody();
                if (newTransaction != null) {
                    logger.info("Вызов метода exchange для транзакции - {}", newTransaction);

                    userService.createExchangeTransaction(newTransaction);
                }
            } catch (Exception ex) {
                qiwiErrorsRepository.save(QiwiRunnerError.builder()
                        .error(ex.toString())
                        .fullException(ex.fillInStackTrace().toString())
                        .requestType(RequestTypes.EXCHANGE)
                        .build());
            }
        };

        ChangeStreamRequest<Transaction> request = ChangeStreamRequest.builder(messageListener)
                .collection(TRANSACTIONS)
                .fullDocumentLookup(FullDocument.UPDATE_LOOKUP)
                .filter(excTransaction)
                .build();

        messageListenerContainer.start();
        return messageListenerContainer.register(request, Transaction.class);
    }
}
