package net.vintagepay.tasks;

import net.vintagepay.models.QiwiRunnerError;
import net.vintagepay.models.RequestTypes;
import net.vintagepay.models.Gate;
import net.vintagepay.models.transaction.Transaction;
import net.vintagepay.repositories.GatesRepository;
import net.vintagepay.repositories.ManualRepository;
import net.vintagepay.repositories.QiwiErrorsRepository;
import net.vintagepay.services.RunnerService;
import net.vintagepay.services.UserService;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

import static net.vintagepay.models.transaction.Status.WAITING;

@Component
public class QiwiTasks {
    private static final Logger logger = LoggerFactory.getLogger(QiwiTasks.class);
    private final QiwiErrorsRepository qiwiErrorsRepository;

    private final ManualRepository manualRepository;
    private final RunnerService qiwiService;
    private final UserService userService;
    private final GatesRepository gatesRepository;

    public QiwiTasks(ManualRepository manualRepository, @Qualifier("qiwiServiceImpl") RunnerService qiwiService,
                     GatesRepository gatesRepository, QiwiErrorsRepository qiwiErrorsRepository, UserService userService) {
        this.manualRepository = manualRepository;
        this.qiwiService = qiwiService;
        this.gatesRepository = gatesRepository;
        this.qiwiErrorsRepository = qiwiErrorsRepository;
        this.userService = userService;
    }

    @Async
    public void pay() {
        try {
            List<Transaction> transactions = new ArrayList<>();
            Transaction tr;
            List<ObjectId> gatesId = manualRepository.findQiwiGates()
                    .stream()
                    .map(Gate::getId)
                    .collect(Collectors.toList());

            try {
                tr = manualRepository.findAndModifyQiwiTransaction(gatesId);
            } catch (Exception ex) {
                tr = Transaction.builder().build();
            }


            while (tr != null) {
                transactions.add(tr);

                try {
                    tr = manualRepository.findAndModifyQiwiTransaction(gatesId);
                } catch (Exception ex) {
                    tr = Transaction.builder().build();
                    continue;
                }
            }

            CompletableFuture<?>[] tasks = transactions
                    .stream()
                    .map(transaction -> {
                        try {
                            logger.info("Вызов метода withdraw для транзакции - {}", transaction);

                            if (transaction.getGateId() != null) {
                                final var optionalGate = gatesRepository.findById(transaction.getGateId());

                                if (!optionalGate.isPresent())
                                    return CompletableFuture.completedFuture(null);

                                qiwiService.withdraw(transaction, optionalGate.get());
                                return CompletableFuture.completedFuture(transaction);
                            }
                            return CompletableFuture.completedFuture(null);
                        } catch (Exception exc) {
                            saveTransactionAfterException(transaction, 0, WAITING);

                            logger.error("Во время выполнения запроса на вывод возникла ошибка!", exc);
                            return CompletableFuture.completedFuture(null);
                        }
                    })
                    .toArray(CompletableFuture[]::new);

            CompletableFuture.allOf(tasks)
                    .get();
        } catch (Exception ex) {
            qiwiErrorsRepository.save(QiwiRunnerError.builder()
                    .error(ex.toString())
                    .fullException(ex.fillInStackTrace().toString())
                    .requestType(RequestTypes.WITHDRAW)
                    .build());
        }
    }

    @Async
    public void exchange() {
        List<Transaction> transactionsExchange = manualRepository.findExchangeTransactions();

        transactionsExchange
                .forEach(transaction -> {
                    if (transaction != null) {
                        logger.info("Вызов метода exchange для транзакции - {}", transaction);

                        userService.createExchangeTransaction(transaction);
                    }
                });
    }

    @Scheduled(fixedDelay = 10000, initialDelay = 1000)
    public void payAfterError() {
        try {
            List<ObjectId> gatesId = manualRepository.findQiwiGates()
                    .stream()
                    .map(Gate::getId)
                    .collect(Collectors.toList());

            List<Transaction> transactions = manualRepository.findQiwiTransactionAfterError(gatesId);

            CompletableFuture<?>[] tasks = transactions
                    .stream()
                    .map(transaction -> {
                        try {
                            logger.info("Вызов метода withdraw для транзакции - {}", transaction);

                            if (transaction.getGateId() != null) {
                                final var optionalGate = gatesRepository.findById(transaction.getGateId());

                                if (!optionalGate.isPresent())
                                    return CompletableFuture.completedFuture(null);

                                qiwiService.withdraw(transaction, optionalGate.get());
                                return CompletableFuture.completedFuture(transaction);
                            }
                            return CompletableFuture.completedFuture(null);
                        } catch (Exception exc) {
                            saveTransactionAfterException(transaction, 0, WAITING);

                            logger.error("Во время выполнения запроса на вывод возникла ошибка!", exc);
                            return CompletableFuture.completedFuture(null);
                        }
                    })
                    .toArray(CompletableFuture[]::new);

            CompletableFuture.allOf(tasks)
                    .get();
        } catch (Exception ex) {
            qiwiErrorsRepository.save(QiwiRunnerError.builder()
                    .error(ex.toString())
                    .fullException(ex.fillInStackTrace().toString())
                    .requestType(RequestTypes.WITHDRAW)
                    .build());
        }
    }

    @Scheduled(fixedDelay = 10000, initialDelay = 1000)
    public void checkTransactions() {
        try {
            List<ObjectId> gatesId = manualRepository.findQiwiGates()
                    .stream()
                    .map(Gate::getId)
                    .collect(Collectors.toList());

            List<Transaction> transactions = manualRepository.findQiwiTransactions(gatesId);

            CompletableFuture<?>[] tasks = transactions
                    .parallelStream()
                    .map(transaction -> {
                        try {
                            final var gateOptional = gatesRepository.findById(transaction.getGateId());
                            if (gateOptional.isPresent()) {
                                qiwiService.checkTransaction(transaction, gateOptional.get());

                                return CompletableFuture.completedFuture(transaction);
                            } else {
                                logger.error("Во время выполнения запроса на проверку статуса возникла ошибка! Гейт для транзакции не найден! {}", transaction);

                                return null;
                            }
                        } catch (Exception exc) {
                            logger.error("Во время выполнения запроса на проверку статуса возникла ошибка!", exc);

                            return CompletableFuture.completedFuture(null);
                        }
                    })
                    .toArray(CompletableFuture[]::new);

            CompletableFuture.allOf(tasks)
                    .get();
        } catch (Exception ex) {
            qiwiErrorsRepository.save(QiwiRunnerError.builder()
                    .error(ex.toString())
                    .fullException(ex.fillInStackTrace().toString())
                    .requestType(RequestTypes.CHECK)
                    .build());
        }
    }

    @Scheduled(fixedDelay = 60000, initialDelay = 1000)
    public void checkManualTransactions() {
        try {
            List<ObjectId> gatesId = manualRepository.findQiwiGates()
                    .stream()
                    .map(Gate::getId)
                    .collect(Collectors.toList());

            List<Transaction> transactions = manualRepository.findQiwiManualTransactions(gatesId);

            CompletableFuture<?>[] tasks = transactions
                    .parallelStream()
                    .map(transaction -> {
                        try {
                            final var gateOptional = gatesRepository.findById(transaction.getGateId());
                            if (gateOptional.isPresent()) {
                                qiwiService.checkTransaction(transaction, gateOptional.get());

                                return CompletableFuture.completedFuture(transaction);
                            } else {
                                logger.error("Во время выполнения запроса на проверку статуса возникла ошибка! Гейт для транзакции не найден! {}", transaction);

                                return null;
                            }
                        } catch (Exception exc) {
                            logger.error("Во время выполнения запроса на проверку статуса возникла ошибка!", exc);

                            return CompletableFuture.completedFuture(null);
                        }
                    })
                    .toArray(CompletableFuture[]::new);

            CompletableFuture.allOf(tasks)
                    .get();
        } catch (Exception ex) {
            qiwiErrorsRepository.save(QiwiRunnerError.builder()
                    .error(ex.toString())
                    .fullException(ex.fillInStackTrace().toString())
                    .requestType(RequestTypes.CHECK)
                    .build());
        }
    }

    @Scheduled(fixedDelay = 1800000)
    public void ping() {
        qiwiService.ping();
    }

    public void saveTransactionAfterException(Transaction transaction, int stage, int status) {
        transaction.setStage(stage);
        transaction.setStatus(status);

        manualRepository.saveTransaction(transaction);
    }
}
