package net.vintagepay.repositories;

import net.vintagepay.models.Count;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface CounterRepository extends MongoRepository<Count, ObjectId> {

}
