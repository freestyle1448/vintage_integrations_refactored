package net.vintagepay.repositories;

import net.vintagepay.models.Currency;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface CurrenciesRepository extends MongoRepository<Currency, ObjectId> {
    Currency findByCurrency(String currency);
}
