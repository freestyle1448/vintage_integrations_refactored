package net.vintagepay.repositories;

import net.vintagepay.models.Gate;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface GatesRepository extends MongoRepository<Gate, ObjectId> {
}

