package net.vintagepay.repositories;

import net.vintagepay.models.Account;
import net.vintagepay.models.Gate;
import net.vintagepay.models.transaction.Balance;
import net.vintagepay.models.transaction.Status;
import net.vintagepay.models.transaction.Transaction;
import net.vintagepay.models.transaction.Type;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.FindAndModifyOptions;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ManualRepository {
    public static final String GATE_ID = "gateId";
    public static final String STATUS = "status";
    public static final String STAGE = "stage";
    private static final String BALANCE_AMOUNT = "balance.amount";
    private static final String BALANCE_CURRENCY = "balance.currency";
    public static final String TRANSACTION_NUMBER = "transactionNumber";

    private final MongoTemplate mongoTemplate;

    public ManualRepository(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }

    public Account findAndModifyAccountAdd(String accountId, Balance balance) {
        var findAndModifyAccount = new Query(Criteria
                .where("accountId").is(accountId)
                .and(BALANCE_CURRENCY).is(balance.getCurrency()));
        var update = new Update();
        update.inc(BALANCE_AMOUNT, balance.getAmount());

        return mongoTemplate.findAndModify(findAndModifyAccount, update, FindAndModifyOptions.options().returnNew(true), Account.class);
    }

    public void saveTransaction(Transaction transaction) {
        var findAndModifyTransaction = new Query(Criteria
                .where(TRANSACTION_NUMBER).is(transaction.getTransactionNumber()));
        var update = new Update();
        update.set(STATUS, transaction.getStatus());
        update.set(STAGE, transaction.getStage());

        if (transaction.getErrorReason() != null) {
            update.set("errorReason", transaction.getErrorReason());
            update.set("systemError", transaction.getSystemError());
        }
        if (transaction.getEndDate() != null) {
            update.set("endDate", transaction.getEndDate());
        }

        mongoTemplate.findAndModify(findAndModifyTransaction, update, Transaction.class);
    }

    public Account findAndModifyAccountSub(String accountId, Balance balance) {
        var findAndModifyAccount = new Query(Criteria
                .where("accountId").is(accountId)
                .and(BALANCE_CURRENCY).is(balance.getCurrency()));
        var update = new Update();
        update.inc(BALANCE_AMOUNT, -balance.getAmount());

        return mongoTemplate.findAndModify(findAndModifyAccount, update, FindAndModifyOptions.options().returnNew(true), Account.class);
    }

    public Gate findAndModifyGateAdd(ObjectId gateId, Balance balance) {
        Query findAndModifyGate = new Query(Criteria
                .where("_id").is(gateId)
                .and(BALANCE_CURRENCY).is(balance.getCurrency()));
        Update update = new Update();
        update.inc(BALANCE_AMOUNT, balance.getAmount());

        return mongoTemplate.findAndModify(findAndModifyGate, update, Gate.class);
    }

    public void findAndModifyGateSet(ObjectId gateId, Balance balance) {
        Query findAndModifyGate = new Query(Criteria
                .where("_id").is(gateId));
        Update update = new Update();
        update.set(BALANCE_AMOUNT, balance.getAmount());

        mongoTemplate.findAndModify(findAndModifyGate, update, Gate.class);
    }

    public List<Gate> findQiwiGates() {
        var find = new Query(Criteria
                .where("group").is("qiwi"));

        return mongoTemplate.find(find, Gate.class);
    }

    public List<Transaction> findQiwiTransactions(List<ObjectId> gates) {
        Query query = new Query(Criteria
                .where(STATUS).is(Status.IN_PROCESS)
                .and(STAGE).is(1)
                .and(GATE_ID).in(gates)
                .and("type").is(Type.USER_OUT)
        );

        return mongoTemplate.find(query, Transaction.class);
    }

    public List<Transaction> findQiwiManualTransactions(List<ObjectId> gates) {
        Query query = new Query(Criteria
                .where(STATUS).is(Status.MANUAL)
                .and(STAGE).is(0)
                .and(GATE_ID).in(gates)
                .and("type").is(Type.USER_OUT)
        );

        return mongoTemplate.find(query, Transaction.class);
    }

    public List<Transaction> findQiwiTransactionAfterError(List<ObjectId> gates) {
        Query query = new Query(Criteria
                .where(STATUS).is(Status.IN_PROCESS)
                .and(STAGE).is(3)
                .and(GATE_ID).in(gates)
                .and("type").is(Type.USER_OUT)
        );

        return mongoTemplate.find(query, Transaction.class);
    }

    public List<Transaction> findExchangeTransactions() {
        final var excTransaction = new Query(Criteria
                .where(STATUS).is(Status.WAITING)
                .and(STAGE).is(0)
                .and(TRANSACTION_NUMBER).ne(null)
                .and("type").is(Type.USER_EXCHANGE));

        return mongoTemplate.find(excTransaction, Transaction.class);
    }

    public Transaction findAndModifyQiwiTransaction(List<ObjectId> gates) {
        Query query = new Query(Criteria
                .where(STATUS).is(Status.WAITING)
                .and(GATE_ID).in(gates)
                .and(STAGE).is(0)
                .and(TRANSACTION_NUMBER).ne(null)
                .and("type").is(Type.USER_OUT));
        Update update = new Update();
        update.inc(STAGE, 1);

        return mongoTemplate.findAndModify(query, update,
                FindAndModifyOptions.options().returnNew(true), Transaction.class);
    }

    public Transaction findAndModifyQiwiTransaction(ObjectId objectId) {
        Query query = new Query(Criteria
                .where("_id").is(objectId));
        Update update = new Update();
        update.inc(STAGE, 1);

        return mongoTemplate.findAndModify(query, update,
                FindAndModifyOptions.options().returnNew(true), Transaction.class);
    }
}
