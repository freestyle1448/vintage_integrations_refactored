package net.vintagepay.repositories;

import net.vintagepay.models.QiwiRunnerError;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface QiwiErrorsRepository extends MongoRepository<QiwiRunnerError, ObjectId> {
}
