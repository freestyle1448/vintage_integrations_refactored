package net.vintagepay.services;

import net.vintagepay.models.Gate;
import net.vintagepay.models.transaction.Transaction;

public interface RunnerService {
    void declineTransaction(Transaction transaction, String errorCause, String systemError);

    void withdraw(Transaction transaction, Gate gate);

    void ping();

    void checkTransaction(Transaction transaction, Gate gate);
}
