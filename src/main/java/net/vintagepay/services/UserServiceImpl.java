package net.vintagepay.services;

import com.mongodb.MongoException;
import net.vintagepay.exception.NotEnoughMoneyException;
import net.vintagepay.exception.NotFoundException;
import net.vintagepay.models.transaction.Transaction;
import net.vintagepay.repositories.ManualRepository;
import net.vintagepay.tasks.QiwiTasks;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Recover;
import org.springframework.retry.annotation.Retryable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import static net.vintagepay.models.transaction.Status.*;

@Service
@Async
public class UserServiceImpl implements UserService {
    private static final Logger logger = LoggerFactory.getLogger(QiwiTasks.class);

    private final SystemService systemService;
    private final ManualRepository manualRepository;

    public UserServiceImpl(SystemService systemService, ManualRepository manualRepository) {
        this.systemService = systemService;
        this.manualRepository = manualRepository;
    }

    @Recover
    public void recoverExchange(MongoException mongoException, Transaction transaction) {
        systemService.saveTransaction(transaction, 0, WAITING);
        logger.error("Retry не справился {} \n {}", mongoException, transaction);
    }

    @Transactional
    @Retryable(value = {MongoException.class, DataAccessException.class},
            maxAttempts = 11500,
            backoff = @Backoff(delay = 5))
    @Override
    public void createExchangeTransaction(Transaction transaction) {
        final var senderAccount = manualRepository.findAndModifyAccountSub(transaction.getSenderCredentials().getAccount()
                , transaction.getAmount());

        if (senderAccount == null) {
            transaction.setStatus(DENIED);
            transaction.setStage(2);
            transaction.setSystemError("Аккаунт отправителя не найден!");
            transaction.setErrorReason("Аккаунт отправителя не найден!");

            throw new NotFoundException("Аккаунт отправителя не найден!");
        }
        if (senderAccount.getBalance().getAmount() < 0) {
            transaction.setStatus(DENIED);
            transaction.setStage(2);
            transaction.setSystemError("INSUFFICIENT_FUNDS");
            transaction.setErrorReason("INSUFFICIENT_FUNDS");

            throw new NotEnoughMoneyException("INSUFFICIENT_FUNDS");
        }

        if (manualRepository.findAndModifyAccountAdd(transaction.getReceiverCredentials().getAccount()
                , transaction.getAmount()) == null) {
            transaction.setStatus(DENIED);
            transaction.setStage(2);
            transaction.setSystemError("Аккаунт получателя не найден!");
            transaction.setErrorReason("Аккаунт получателя не найден!");

            throw new NotFoundException("Аккаунт получателя не найден!");
        }

        systemService.saveTransaction(transaction, 2, SUCCESS);
    }
}
