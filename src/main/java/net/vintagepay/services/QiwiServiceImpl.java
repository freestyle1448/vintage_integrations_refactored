package net.vintagepay.services;

import net.vintagepay.app.QiwiProperties;
import net.vintagepay.models.Account;
import net.vintagepay.models.Gate;
import net.vintagepay.models.QiwiBean;
import net.vintagepay.models.QiwiHTTPRequest;
import net.vintagepay.models.qiwi.PrivateCredentials;
import net.vintagepay.models.qiwi.pay.Request;
import net.vintagepay.models.transaction.Transaction;
import net.vintagepay.repositories.ManualRepository;
import net.vintagepay.tasks.QiwiTasks;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
@Async
public class QiwiServiceImpl implements RunnerService {
    private static final Logger logger = LoggerFactory.getLogger(QiwiTasks.class);

    private final SystemService systemService;
    private final ManualRepository manualRepository;
    private final QiwiHTTPRequest qiwiHTTPRequest;
    private final QiwiBean qiwiBean;

    public QiwiServiceImpl(SystemService systemService, ManualRepository manualRepository, QiwiHTTPRequest qiwiHTTPRequest, QiwiBean qiwiBean) {
        this.systemService = systemService;

        this.manualRepository = manualRepository;
        this.qiwiHTTPRequest = qiwiHTTPRequest;
        this.qiwiBean = qiwiBean;
    }

    public static int getCurrency(String currency) {
        switch (currency) {
            case "RUB":
                return 643;
            case "EUR":
                return 978;
            default:
                return 0;
        }
    }

    @Override
    public void withdraw(Transaction transaction, Gate gate) {
        logger.info("Обработка транзакции на вывод");
        if (transaction.getStage() != 3) {
            Account result = systemService.accountSub(transaction);

            if (result == null) {
                logger.error("При попытке списания средств с аккаунта произошла ошибка!\n {}", transaction);
                return;
            }

            logger.info("Списание с аккаунта успешно");
        }

        Request request = null;

        var receiverCredentials = transaction.getReceiverCredentials();
        var qiwiCredentials = gate.getQiwiCredentials();

        var terminalId = qiwiCredentials.getTerminal_id();
        var password = qiwiCredentials.getPassword();
        var serviceCards = QiwiProperties.service_id_qiwi_cards;
        var serviceWallet = QiwiProperties.service_id_qiwi_wallets;
        var privateCredentials = PrivateCredentials.builder()
                .certificate(qiwiCredentials.getCertificate())
                .privateKey(qiwiCredentials.getPrivateKey())
                .build();

        if (receiverCredentials.getDestination().equals("card")) {
            request = Request.buildRequest("pay",
                    terminalId,
                    password,
                    "+79999999999",
                    receiverCredentials.getCardNumber(),
                    transaction.getAmount().getCurrency(),
                    gate.getBalance().getCurrency(),
                    String.valueOf(transaction.getAmount().getAmount().doubleValue() / 100),
                    serviceCards,
                    String.valueOf(transaction.getTransactionNumber()),
                    privateCredentials);
        }
        if (receiverCredentials.getDestination().equals("wallet")) {
            if (receiverCredentials.getPhone().charAt(0) == '+')
                receiverCredentials.setPhone(receiverCredentials.getPhone().replace("+", ""));

            request = Request.buildRequest("pay",
                    terminalId,
                    password,
                    receiverCredentials.getPhone(),
                    transaction.getAmount().getCurrency(),
                    gate.getBalance().getCurrency(),
                    String.valueOf(transaction.getAmount().getAmount().doubleValue() / 100),
                    serviceWallet,
                    String.valueOf(transaction.getTransactionNumber()),
                    privateCredentials);

        }
        qiwiHTTPRequest.withdrawQiwi(request)
                .whenComplete((qiwiResponse, throwable) -> {
                    if (qiwiResponse != null && throwable == null) {
                        qiwiBean.handleWithdraw(qiwiResponse, transaction);
                    } else {
                        declineTransaction(transaction, "Ошибка при  проведении платежа", "Ошибка при получении ответа от Qiwi");

                        logger.info("Списание средств неуспешно для транзакции - {}, сообщение - Не удалось получить ответ от QIWI"
                                , transaction);
                    }
                });
    }

    @Override
    public void checkTransaction(Transaction transaction, Gate gate) {
        Request request = null;
        net.vintagepay.models.qiwi.checkStatus.Request check = null;

        var receiverCredentials = transaction.getReceiverCredentials();
        var qiwiCredentials = gate.getQiwiCredentials();

        var terminalId = qiwiCredentials.getTerminal_id();
        var password = qiwiCredentials.getPassword();
        var serviceCards = QiwiProperties.service_id_qiwi_cards;

        var privateCredentials = PrivateCredentials.builder()
                .certificate(qiwiCredentials.getCertificate())
                .privateKey(qiwiCredentials.getPrivateKey())
                .build();

        if (receiverCredentials.getDestination().equals("card")) {
            request = Request.buildRequest("pay",
                    terminalId,
                    password,
                    "+79999999999",
                    receiverCredentials.getCardNumber(),
                    transaction.getAmount().getCurrency(),
                    gate.getBalance().getCurrency(),
                    String.valueOf(transaction.getAmount().getAmount().doubleValue() / 100),
                    serviceCards,
                    String.valueOf(transaction.getTransactionNumber()),
                    privateCredentials);
        }
        if (receiverCredentials.getDestination().equals("wallet")) {
            check = net.vintagepay.models.qiwi.checkStatus.Request.buildRequest(receiverCredentials.getPhone(),
                    String.valueOf(transaction.getTransactionNumber()),
                    password,
                    "pay",
                    terminalId,
                    privateCredentials);
        }

        qiwiHTTPRequest.checkTransactionQiwi(check, request, transaction.getTransactionNumber())
                .whenComplete((qiwiResponse, throwable) -> {
                    if (qiwiResponse != null && throwable == null) {
                        qiwiBean.handleCheckTransaction(qiwiResponse, transaction);
                    }
                });
    }

    @Override
    public void ping() {
        final var requests = new ArrayList<net.vintagepay.models.qiwi.ping.Request>();
        final var gates = manualRepository.findQiwiGates();

        for (Gate gate : gates) {
            final var qiwiCredentials = gate.getQiwiCredentials();
            net.vintagepay.models.qiwi.ping.Request requestPingQiwi = net.vintagepay.models.qiwi.ping.Request.buildRequest(
                    qiwiCredentials.getPassword(),
                    "ping",
                    qiwiCredentials.getTerminal_id());
            requestPingQiwi.setObjectId(gate.getId());
            requestPingQiwi.setCurrency(String.valueOf(getCurrency(gate.getBalance().getCurrency())));
            PrivateCredentials privateCredentials = PrivateCredentials.builder()
                    .certificate(gate.getQiwiCredentials().getCertificate())
                    .privateKey(gate.getQiwiCredentials().getPrivateKey())
                    .build();
            requestPingQiwi.setPrivateCredentials(privateCredentials);

            requests.add(requestPingQiwi);
        }

        for (net.vintagepay.models.qiwi.ping.Request request : requests) {
            qiwiHTTPRequest.ping(request).whenComplete((response, throwable) -> {
                if (response != null && throwable == null) {
                    qiwiBean.handlePing(response);
                }
            });
        }
    }

    @Override
    public void declineTransaction(Transaction transaction, String errorCause, String systemError) {
        systemService.declineTransaction(transaction, errorCause, systemError);
    }
}
