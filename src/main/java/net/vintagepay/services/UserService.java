package net.vintagepay.services;

import net.vintagepay.models.transaction.Transaction;

public interface UserService {
    void createExchangeTransaction(Transaction transaction);
}
