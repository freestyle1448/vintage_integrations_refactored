package net.vintagepay.services;

import com.mongodb.MongoException;
import lombok.RequiredArgsConstructor;
import net.vintagepay.exception.NotEnoughMoneyException;
import net.vintagepay.exception.NotFoundException;
import net.vintagepay.models.Account;
import net.vintagepay.models.Answer;
import net.vintagepay.models.transaction.Balance;
import net.vintagepay.models.transaction.Status;
import net.vintagepay.models.transaction.Transaction;
import net.vintagepay.models.transaction.UserCredentials;
import net.vintagepay.repositories.ManualRepository;
import net.vintagepay.repositories.TransactionsRepository;
import net.vintagepay.tasks.QiwiTasks;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Recover;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.concurrent.CompletableFuture;

import static net.vintagepay.models.RequestTypes.IN;
import static net.vintagepay.models.transaction.Status.*;

@RequiredArgsConstructor
@Service
public class SystemServiceImpl implements SystemService {
    private static final Logger logger = LoggerFactory.getLogger(QiwiTasks.class);

    private final TransactionsRepository transactionsRepository;
    private final ManualRepository manualRepository;

    @Recover
    public Account recoverAccountSub(MongoException mongoException, Transaction transaction) {
        saveTransaction(transaction, 0, WAITING);
        logger.error("Retry не справился {} \n {}", mongoException, transaction);

        return null;
    }

    @Recover
    public CompletableFuture<Answer> recoverAccountAdd(Exception ex, Balance balance, String account) {
        return CompletableFuture.completedFuture(Answer.builder()
                .status("ERR")
                .err(ex.getLocalizedMessage())
                .build());
    }

    @Transactional
    @Retryable(value = {MongoException.class, DataAccessException.class},
            maxAttempts = 500,
            backoff = @Backoff(delay = 5))
    @Override
    public CompletableFuture<Answer> accountAdd(Balance balance, String account) {
        final var receiverAccount = manualRepository.findAndModifyAccountAdd(account
                , balance);

        if (true) {
            throw new MongoException("test");
        }

        if (receiverAccount == null) {
            throw new NotFoundException("Аккаунт с указанным ID не найден!");
        }

        final var transaction = Transaction.builder()
                .startDate(new Date())
                .endDate(new Date())
                .status(SUCCESS)
                .stage(2)
                .amount(balance)
                .finalAmount(balance)
                .type(IN)
                .receiverCredentials(UserCredentials.builder()
                        .account(account)
                        .build())
                .build();

        transactionsRepository.save(transaction);

        return CompletableFuture.completedFuture(Answer.builder()
                .status("OK")
                .amount(receiverAccount.getBalance().getAmount())
                .build());
    }

    @Transactional
    @Retryable(value = {MongoException.class, DataAccessException.class},
            maxAttempts = 11500,
            backoff = @Backoff(delay = 5))
    @Override
    public Account accountSub(Transaction transaction) {
        final var senderAccount = manualRepository.findAndModifyAccountSub(transaction.getSenderCredentials().getAccount()
                , transaction.getFinalAmount());

        if (senderAccount == null) {
            transaction.setStatus(DENIED);
            transaction.setStage(2);
            transaction.setSystemError("Аккаунт отправителя не найден!");
            transaction.setErrorReason("Аккаунт отправителя не найден!");

            throw new NotFoundException("Аккаунт отправителя не найден!");
        }
        if (senderAccount.getBalance().getAmount() < 0) {
            transaction.setStatus(DENIED);
            transaction.setStage(2);
            transaction.setSystemError("INSUFFICIENT_FUNDS");
            transaction.setErrorReason("INSUFFICIENT_FUNDS");

            throw new NotEnoughMoneyException("INSUFFICIENT_FUNDS");
        }

        return senderAccount;
    }

    @Override
    public void saveTransaction(Transaction transaction, int stage, int status) {
        if (status == SUCCESS) {
            transaction.setEndDate(new Date());
        }
        transaction.setStatus(status);
        transaction.setStage(stage);

        manualRepository.saveTransaction(transaction);
    }

    @Override
    public void saveTransaction(Transaction transaction, int stage, int status, String errorReason, String systemError) {
        if (status == DENIED || status == MANUAL) {
            transaction.setEndDate(new Date());
            transaction.setErrorReason(errorReason);
            transaction.setSystemError(systemError);
        }
        transaction.setStatus(status);
        transaction.setStage(stage);

        manualRepository.saveTransaction(transaction);
    }

    @Override
    public void confirmTransaction(Transaction transaction) {
        if (transaction != null) {
            saveTransaction(transaction, 2, Status.SUCCESS);
            logger.info("Транзакция успешно принята(confirmTransaction) - {}"
                    , transaction);
        } else {
            logger.error("Для подтверждения транзакции передали не существующую транзакцию");
        }
    }

    @Transactional
    @Retryable(value = {MongoException.class, DataAccessException.class},
            maxAttempts = 11500,
            backoff = @Backoff(delay = 5))
    @Override
    public void declineTransaction(Transaction transaction, String errorCause, String systemError) {
        final var modifyAccount = manualRepository.findAndModifyAccountAdd(transaction.getSenderCredentials().getAccount(), transaction.getFinalAmount());

        if (modifyAccount == null) {
            throw new NotFoundException("Аккаунт для возврата не найден!");
        }

        saveTransaction(transaction, 1, Status.DENIED, errorCause, systemError);
    }
}
