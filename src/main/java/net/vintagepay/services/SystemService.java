package net.vintagepay.services;

import net.vintagepay.models.Account;
import net.vintagepay.models.Answer;
import net.vintagepay.models.transaction.Balance;
import net.vintagepay.models.transaction.Transaction;

import java.util.concurrent.CompletableFuture;

public interface SystemService {
    CompletableFuture<Answer> accountAdd(Balance balance, String account);

    Account accountSub(Transaction transaction);

    void saveTransaction(Transaction transaction, int stage, int status);

    void saveTransaction(Transaction transaction, int stage, int status, String errorReason, String systemError);

    void confirmTransaction(Transaction transaction);

    void declineTransaction(Transaction transaction, String errorCause, String systemError);
}
