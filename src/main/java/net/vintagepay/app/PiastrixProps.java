package net.vintagepay.app;

import org.bson.types.ObjectId;

public final class PiastrixProps {
    public static final ObjectId PIASTRIX_GATE_ID = new ObjectId("5cd96d62f64edb30dc0097c4");
    public final static String PRODUCTION_URL = "";
    public final static String DEBUGGING_URL = "https://private-anon-cb6235f96e-piastrix.apiary-proxy.com/withdraw/try";
    public final static String DEBUGGING_URL_CHECK_ACC = "https://private-anon-40fa3a809a-piastrix.apiary-proxy.com/check_account";
    public static final String DEBUGGING_URL_WITHDRAW = "https://private-anon-4ab1de7be7-piastrix.apiary-proxy.com/withdraw/create";
    public static final String DEBUGGING_URL_WITHDRAW_ID = "https://private-anon-4ab1de7be7-piastrix.apiary-proxy.com/withdraw/status";
    public static final String DEBUGGING_URL_SHOP_BALANCE = "https://private-anon-6e26664e4c-piastrix.apiary-proxy.com/shop_balance";
    public final static String MOCK_URL = "https://private-anon-cb6235f96e-piastrix.apiary-mock.com/withdraw/try";

    public final static Integer SHOP_ID = 5;
    public final static String PAYWAY = "yamoney_rub";
    public final static String SECRET_KEY = "SecretKey01";

    public static final String SOURCE_LOG = "PIASTRIX";
}
